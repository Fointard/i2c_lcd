#![no_main]
#![no_std]

use cortex_m_rt::entry;
use i2c_lcd::*;
use microbit::{
	hal::{prelude::*, Temp, Timer, Twim},
	pac::twim0::frequency::FREQUENCY_A,
	Board,
};
#[cfg(feature = "panic-halt")]
use panic_halt as _;
#[cfg(feature = "rtt")]
use panic_rtt_target as _;

#[entry]
fn main() -> ! {
	#[cfg(feature = "rtt")]
	rtt_target::rtt_init_print!();

	let board = Board::take().expect("Failed to take board");
	let mut temp = Temp::new(board.TEMP);
	let mut timer = Timer::new(board.TIMER0);

	let mut lcd = Lcd::new(
		Timer::new(board.TIMER1),
		Twim::new(board.TWIM0, board.i2c_external.into(), FREQUENCY_A::K400),
	)
	.init();

	timer.delay_ms(1000u16);

	"Hello, World!"
		.bytes()
		.for_each(|b| lcd.send_cmd(Command::Write(b)));

	timer.delay_ms(1000u16);

	lcd.send_cmd(Command::SetDDRAMAddress(0x40));
	"Temp: "
		.bytes()
		.for_each(|b| lcd.send_cmd(Command::Write(b)));

	lcd.send_cmd(Command::DisplayControl(
		Display::On,
		Cursor::Off,
		CursorBlink::Off,
	));

	loop {
		lcd.send_cmd(Command::SetDDRAMAddress(0x46));

		let mut buf = ryu::Buffer::new();
		let measure = buf.format_finite(temp.measure().to_num::<f32>());

		#[cfg(feature = "rtt")]
		rtt_target::rprintln!("measurement: {}", measure);

		measure
			.bytes()
			.chain(" ".bytes())
			.for_each(|c| lcd.send_cmd(Command::Write(c)));

		timer.delay_ms(500u16);
	}
}
