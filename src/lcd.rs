pub mod lcd_cfg;
pub mod lcd_cmd;

use core::fmt::Debug;

use embedded_hal::blocking::{delay::DelayUs, i2c::Write};
use lcd_cfg::*;
use lcd_cmd::Command;

pub struct Lcd<I2C, TIMER> {
	backlight: Backlight,
	font: Font,
	i2c_address: u8,
	i2c_bus: I2C,
	lines: Lines,
	timer: TIMER,
}

impl<I2C: Write<Error = impl Debug>, TIMER: DelayUs<u16>> Lcd<I2C, TIMER> {
	const DEFAULT_I2C_ADDRESS: u8 = 0x27;

	fn get_mode_cmd(&self, data_len: DataLength) -> u8 {
		u8::from(Command::FunctionSet(data_len, self.lines, self.font))
	}

	fn i2c_pulse(&mut self, data: u8) {
		for half in 1..=2 {
			let _result = self.i2c_bus.write(
				self.i2c_address,
				&[data | if half == 1 { Enable::On } else { Enable::Off } as u8],
			);
			self.timer.delay_us(1000u16);

			#[cfg(feature = "rtt")]
			if let Err(e) = _result {
				rtt_target::rprintln!("Error while sending half {}: {:?}", half, e);
			}
		}

		self.timer.delay_us(4000u16);
	}

	pub fn init(mut self) -> Self {
		self.timer.delay_us(45000u16);

		self.set_4bit_mode();

		self.send_cmd(Command::FunctionSet(
			DataLength::FourBits,
			Lines::Two,
			Font::FiveByEight,
		));

		self.send_cmd(Command::DisplayControl(
			Display::Off,
			Cursor::Off,
			CursorBlink::Off,
		));

		self.send_cmd(Command::ClearDisplay);

		self.send_cmd(Command::EntryModeSet(
			CursorMoveDirection::Right,
			DisplayShift::Off,
		));

		self.send_cmd(Command::DisplayControl(
			Display::On,
			Cursor::On,
			CursorBlink::On,
		));

		self
	}

	pub fn new(timer: TIMER, i2c_bus: I2C) -> Self {
		Self {
			backlight: Backlight::On,
			font: Font::FiveByEight,
			i2c_address: Self::DEFAULT_I2C_ADDRESS,
			i2c_bus,
			lines: Lines::Two,
			timer,
		}
	}

	pub fn send_cmd(&mut self, cmd: Command) {
		let rs_bit = cmd.rs_bit();
		let data = u8::from(cmd);

		let msb = data & 0xF0 | rs_bit | self.backlight as u8;
		let lsb = data << 4 & 0xF0 | rs_bit | self.backlight as u8;

		self.i2c_pulse(msb);
		self.i2c_pulse(lsb);

		self.timer.delay_us(1600u16);
	}

	fn set_4bit_mode(&mut self) {
		let mode_8b = self.get_mode_cmd(DataLength::EightBits);
		let mode_4b = self.get_mode_cmd(DataLength::FourBits);

		let delays = &[4150, 150, 50, 50];

		for (idx, delay) in delays.iter().enumerate() {
			self.i2c_pulse(if idx != 3 { mode_8b } else { mode_4b });
			self.timer.delay_us(*delay);
		}
	}

	pub fn set_address<I: Into<u8>>(mut self, address: I) -> Self {
		self.i2c_address = address.into();
		self
	}

	pub fn set_backlight(mut self, backlight: Backlight) -> Self {
		self.backlight = backlight;
		self
	}

	pub fn set_font(mut self, font: Font) -> Self {
		self.font = font;
		self
	}

	pub fn set_lines(mut self, lines: Lines) -> Self {
		self.lines = lines;
		self
	}
}
