#![no_std]

#[cfg(all(feature = "panic-halt", feature = "rtt"))]
compile_error!("`panic-halt` and `rtt` features are mutually exclusive");

pub mod lcd;

pub use lcd::{lcd_cfg::*, lcd_cmd::Command, Lcd};
