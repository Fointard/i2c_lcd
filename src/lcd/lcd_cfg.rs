#[derive(Clone, Copy)]
pub enum Backlight {
	Off = 0x00,
	On = 0x08,
}

pub enum Cursor {
	Off = 0x00,
	On = 0x02,
}

pub enum CursorBlink {
	Off = 0x00,
	On = 0x01,
}

pub enum CursorMoveDirection {
	Left = 0x00,
	Right = 0x02,
}

pub enum DataLength {
	FourBits = 0x00,
	EightBits = 0x10,
}

pub enum Display {
	Off = 0x00,
	On = 0x04,
}

pub enum DisplayShift {
	Off = 0x00,
	On = 0x01,
}

pub enum Enable {
	Off = 0x00,
	On = 0x04,
}

#[derive(Clone, Copy)]
pub enum Font {
	FiveByEight = 0x00,
	FiveByTen = 0x04,
}

#[derive(Clone, Copy)]
pub enum Lines {
	One = 0x00,
	Two = 0x08,
}

pub enum MoveOrShift {
	Move = 0x00,
	Shift = 0x08,
}

pub enum ShiftDirection {
	Left = 0x00,
	Right = 0x04,
}
