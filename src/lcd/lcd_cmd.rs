use super::lcd_cfg::*;

pub enum Command {
	ClearDisplay,
	CursorHome,
	CursorShiftControl(MoveOrShift, ShiftDirection),
	DisplayControl(Display, Cursor, CursorBlink),
	EntryModeSet(CursorMoveDirection, DisplayShift),
	FunctionSet(DataLength, Lines, Font),
	SetCGRAMAddress(u8),
	SetDDRAMAddress(u8),
	Write(u8),
}

impl Command {
	pub(crate) fn rs_bit(&self) -> u8 {
		match self {
			Self::Write(_) => 0x01,
			_ => 0x00,
		}
	}
}

impl From<Command> for u8 {
	fn from(cmd: Command) -> Self {
		match cmd {
			Command::ClearDisplay => 0x01,
			Command::CursorHome => 0x02,
			Command::CursorShiftControl(moveorshift, direction) => {
				0x10 + moveorshift as u8 + direction as u8
			}
			Command::DisplayControl(display, cursor, blink) => {
				0x08 + display as u8 + cursor as u8 + blink as u8
			}
			Command::EntryModeSet(direction, shift) => 0x04 + direction as u8 + shift as u8,
			Command::FunctionSet(length, lines, font) => {
				0x20 + length as u8 + lines as u8 + font as u8
			}
			Command::SetCGRAMAddress(address) => 0x40 + (address & 0x3F),
			Command::SetDDRAMAddress(address) => 0x80 + (address & 0x7F),
			Command::Write(data) => data,
		}
	}
}
