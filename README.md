<div align="center">
<h1>I2C LCD</h1>

![](vid/I2C_LCD.mp4)

</div>

## Description
This repo implements a rust HD44780 driver to pilot an I2C-enabled (PCF8574T) LCD screen using a BBC micro:bit.

## Built With

* [Rust](https://www.rust-lang.org)
* [Embedded-HAL](https://github.com/rust-embedded/embedded-hal)
* [BBC micro:bit](https://tech.microbit.org)
* [Hitachi HD44780 LCD Controller](https://en.wikipedia.org/wiki/Hitachi_HD44780_LCD_controller)
* [PCF8574T I2C I/O expander](https://www.ti.com/lit/ds/symlink/pcf8574.pdf)


<div align="center">

## Contact

[![LinkedIn][linkedin-shield]][linkedin-url]
[![Email][email-shield]][email-url]

</div>

[linkedin-url]: https://www.linkedin.com/in/cyrilmarpaud/
[linkedin-shield]: https://img.shields.io/badge/-LinkedIn-black.svg?style=for-the-badge&logo=linkedin&colorB=grey&logoColor=blue
[email-shield]: https://img.shields.io/badge/-Email-grey?style=for-the-badge&logo=mail.ru
[email-url]: mailto:cyril.marpaud@bashroom.com
